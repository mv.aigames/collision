
#include "CollisionToolEditorModule/Public/CollisionToolEditorModule.h"

IMPLEMENT_GAME_MODULE(FCollisionToolEditor, CollisionToolEditor);

TSharedRef<FWorkspaceItem> FCollisionToolEditor::MenuRoot = FWorkspaceItem::NewGroup(FText::FromString("Menu Root"));

void FCollisionToolEditor::AddMenuExtension(const FMenuExtensionDelegate& extensionDelegate,
                                            FName extensionHook, const TSharedPtr<FUICommandList>& commandList,
                                            EExtensionHook::Position position)
{
    menuExtender->AddMenuExtension(extensionHook, position, commandList, extensionDelegate);
}

void FCollisionToolEditor::MakePulldownMenu(FMenuBarBuilder& menuBuilder)
{
    menuBuilder.AddPullDownMenu(
        FText::FromString("Collision"),
        FText::FromString("Open the Collision Menu"),
        FNewMenuDelegate::CreateRaw(this, &FCollisionToolEditor::FillPulldownMenu),
        "Collision",
        FName(TEXT("CollisionMenu"))
    );
}

void FCollisionToolEditor::FillPulldownMenu(FMenuBuilder& menuBuilder)
{
    menuBuilder.BeginSection("CollisionSection", FText::FromString("World section"));
    menuBuilder.AddMenuSeparator(FName("section_1"));
    menuBuilder.EndSection();

    menuBuilder.BeginSection("CollisionSection", FText::FromString("Space Section"));
    menuBuilder.AddMenuSeparator(FName("section_2"));
    menuBuilder.EndSection();
}

void FCollisionToolEditor::StartupModule()
{
    if (!IsRunningCommandlet())
    {
        FLevelEditorModule& levelEditorModule = FModuleManager::LoadModuleChecked<FLevelEditorModule>("LevelEditor");
        levelEditorMenuExtensibilityManager = levelEditorModule.GetMenuExtensibilityManager();
        menuExtender = MakeShareable(new FExtender);
        menuExtender->AddMenuBarExtension("Window", EExtensionHook::After, NULL, FMenuBarExtensionDelegate::CreateRaw(this, &FCollisionToolEditor::MakePulldownMenu));
        levelEditorMenuExtensibilityManager->AddExtender(menuExtender);

        for (int32 index = 0; index < moduleListeners.Num(); index++)
        {
            moduleListeners[index]->OnStartupModule();
        }
    }
}

void FCollisionToolEditor::ShutdownModule()
{
    UE_LOG(LogTemp, Warning, TEXT("ShutdownModule"));
    for (int32 index = 0; index < moduleListeners.Num(); index++)
    {
        moduleListeners[index]->OnShutdownModule();
    }
}

void FCollisionToolEditor::AddListeners()
{
    //
}


