#pragma once

#include "Modules/ModuleManager.h"
#include "UnrealEd.h"
#include "SlateBasics.h"
#include "SlateExtras.h"
#include "Editor/LevelEditor/Public/LevelEditor.h"
#include "Editor/PropertyEditor/Public/PropertyEditing.h"
#include "IAssetTypeActions.h"
#include "ICollisionModuleInterface.h"

class FCollisionToolEditor: public IModuleInterface
{
    protected:
        TArray<TSharedRef<ICollisionToolModuleInterface>> moduleListeners;
        TSharedPtr<FExtensibilityManager> levelEditorMenuExtensibilityManager;
        TSharedPtr<FExtender> menuExtender;

        static TSharedRef<FWorkspaceItem> MenuRoot;

        void MakePulldownMenu(FMenuBarBuilder& menuBuilder);
        void FillPulldownMenu(FMenuBuilder& menuBuilder);
    
    public:
        virtual void StartupModule() override;
        virtual void ShutdownModule() override;
        virtual void AddListeners();

        static inline FCollisionToolEditor& Get() { return FModuleManager::LoadModuleChecked<FCollisionToolEditor>("CollisionToolEditorModule"); }
        static inline bool IsAvailable() { return FModuleManager::Get().IsModuleLoaded("CollisionToolEditorModule"); }

        void AddMenuExtension(const FMenuExtensionDelegate& extensionDelegate, FName extensionHook, const TSharedPtr<FUICommandList>& commandList = nullptr, EExtensionHook::Position position = EExtensionHook::Before);
        TSharedRef<FWorkspaceItem> GetMenuRoot() { return MenuRoot; } 
};