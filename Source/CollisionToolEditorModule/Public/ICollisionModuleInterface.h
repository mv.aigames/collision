#pragma once

class ICollisionToolModuleInterface
{
    public:
        virtual void OnStartupModule() = 0;
        virtual void OnShutdownModule() = 0;
};