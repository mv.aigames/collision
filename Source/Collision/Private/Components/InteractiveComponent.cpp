// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/InteractiveComponent.h"

#include "CollisionChannelManagment.h"
#include "Entity.h"

UInteractiveComponent::UInteractiveComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
    bWantsInitializeComponent = true;
}

void UInteractiveComponent::InitializeComponent()
{
    Super::InitializeComponent();

    // SetCollisionResponseToChannel(CollisionChannelManagment::GetPLayerSelectionChannel(), ECollisionResponse::ECR_Block);
    SetCollisionObjectType(CollisionChannelManagment::GetPlayerSelectionChannel());

    entity = Cast<AEntity>(GetOwner());
    if (entity)
    {
        OnBeginCursorOver.AddDynamic(this, &UInteractiveComponent::OnCursorOverBegin);
        OnEndCursorOver.AddDynamic(this, &UInteractiveComponent::OnCursorOverEnd);
        // OnClicked.AddDynamic(this, &UInteractiveComponent::OnClickPressed);
        // OnReleased.AddDynamic(this, &UInteractiveComponent::OnClickReleased);
    }
}

void UInteractiveComponent::OnCursorOverBegin(UPrimitiveComponent* primitiveComponent)
{
    UE_LOG(LogTemp, Warning, TEXT("Hover Begin"));
}


void UInteractiveComponent::OnCursorOverEnd(UPrimitiveComponent* primitiveComponent)
{
    UE_LOG(LogTemp, Warning, TEXT("Hover End"));
}

void UInteractiveComponent::OnClickPressed(UPrimitiveComponent* primitiveComponent, FKey ButtonPressed)
{

}

void UInteractiveComponent::OnClickReleased(UPrimitiveComponent* primitiveComponent, FKey ButtonReleased)
{

}

void UInteractiveComponent::AddPropulsion(const FVector& addedPropulsion)
{
    UE_LOG(LogTemp, Warning, TEXT("OnAddPropulsion"));
    Cast<AEntity>(GetOwner())->OnAddPropulsion.Broadcast(addedPropulsion);
}