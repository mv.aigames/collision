// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/OutOfBoundComponent.h"

#include "Components/EntityComponent.h"
#include "Entity.h"
#include "GameModes/LevelGameMode.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

// Sets default values for this component's properties
UOutOfBoundComponent::UOutOfBoundComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UOutOfBoundComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	if (AEntity* entity = Cast<AEntity>(GetOwner()))
	{
		Cast<UEntityComponent>(entity->GetRootComponent())->OnComponentBeginOverlap.AddDynamic(this, &UOutOfBoundComponent::OnDeadZone);
	}
}

void UOutOfBoundComponent::OnDeadZone(UPrimitiveComponent* overlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& Hit)
{
	ALevelGameMode* gameMode = Cast<ALevelGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	gameMode->OnDeadZone.Broadcast();
}