// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/AsteroidBeltComponent.h"
#include "EngineUtils.h"

// Sets default values for this component's properties
UAsteroidBeltComponent::UAsteroidBeltComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UAsteroidBeltComponent::BeginPlay()
{
	Super::BeginPlay();


	TArray<UObject*> meshes;

	EngineUtils::FindOrLoadAssetsByPath(path, meshes, EngineUtils::EAssetToLoad::ATL_Regular);	
	UE_LOG(LogTemp, Warning, TEXT("Creating belt"));
	for (int32 index = 0; index < meshes.Num(); index++)
	{
		UE_LOG(LogTemp, Warning, TEXT("Adding asteroid: %d"), index);
		UStaticMeshComponent* meshComponent = NewObject<UStaticMeshComponent>(GetWorld());
		if (meshComponent)
		{
			UE_LOG(LogTemp, Warning, TEXT("Adding asteroid"));
			FAttachmentTransformRules rules(EAttachmentRule::KeepRelative, true);
			meshComponent->AttachToComponent(this, rules);
			// meshComponent->SetupAttachment(this);
			meshComponent->RegisterComponent();
			UStaticMesh* mesh = Cast<UStaticMesh>(meshes[FMath::RandHelper(meshes.Num())]);
			meshComponent->SetStaticMesh(mesh);
			meshComponent->SetRelativeLocation(FVector(0, 0, 0));
			meshComponents.Add(meshComponent);

			// GetOwner()->AddOwnedComponent(meshComponent);
			// meshComponent->SetVisibility(true);
		}
	}
	// ...
	
}


// Called every frame
void UAsteroidBeltComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	AddRelativeRotation(FRotator(0, 10.0f * DeltaTime, 0.0f));
	// ...
}

