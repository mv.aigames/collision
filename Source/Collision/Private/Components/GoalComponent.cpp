// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/GoalComponent.h"


#include "Entity.h"
#include "CollisionChannelManagment.h"

UGoalComponent::UGoalComponent()
{
    PrimaryComponentTick.bCanEverTick = false;
    SetSimulatePhysics(false);
    
    SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
    // SetCollisionResponseToChannel(CollisionChannelManagment::GetPlayerSelectionChannel(), ECollisionResponse::ECR_Ignore);
}


void UGoalComponent::BeginPlay()
{
    Super::BeginPlay();

    if (AEntity* entity = Cast<AEntity>(GetOwner()))
    {
        UE_LOG(LogTemp, Warning, TEXT("AddDynamic Level completed"));
        OnComponentBeginOverlap.AddDynamic(this, &UGoalComponent::OverlapBegin);
    }
}


void UGoalComponent::OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
    UE_LOG(LogTemp, Warning, TEXT("Level completed"));
    Cast<AEntity>(GetOwner())->OnGoal.Broadcast();
}