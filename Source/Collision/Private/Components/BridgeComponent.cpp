// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/BridgeComponent.h"

#include "CollisionChannelManagment.h"
#include "Components/EntityComponent.h"
#include "Entity.h"

// Sets default values for this component's properties
UBridgeComponent::UBridgeComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...
}

void UBridgeComponent::OnRegister()
{
	Super::OnRegister();

	UE_LOG(LogTemp, Warning, TEXT("OnRegister BridgeComponent"));
}

// Called when the game starts
void UBridgeComponent::BeginPlay()
{
	UE_LOG(LogTemp, Warning, TEXT("BeginPlay"));
	Super::BeginPlay();

	entity = Cast<AEntity>(GetOwner());
	if (!entity)
	{
		UE_LOG(LogTemp, Error, TEXT("BridgeComponent must be child of Entity"));
		SetComponentTickEnabled(false);
		return;
	}

	if (UPrimitiveComponent* pComponent = Cast<UPrimitiveComponent>(GetOwner()->GetRootComponent()))
	{
		pComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
		pComponent->SetCollisionResponseToChannel(CollisionChannelManagment::GetPlayerSelectionChannel(), ECollisionResponse::ECR_Ignore);
		
		pComponent->OnComponentBeginOverlap.AddDynamic(this, &UBridgeComponent::OnBeginOverlap);
		pComponent->OnComponentEndOverlap.AddDynamic(this, &UBridgeComponent::OnEndOverlap);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("Root component must be PrimitiveComponent"));
	}
	
}


// Called every frame
void UBridgeComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	entity->OnDirectionChange.Broadcast(GetHeadingVector(angleDirection));
	// ...
}

void UBridgeComponent::OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 BodyIndex, bool FromSweep, const FHitResult& Hit)
{
	UE_LOG(LogTemp, Warning, TEXT("Bridge enter"));
	AEntity* entryEntity = Cast<AEntity>(OtherActor);
	if (entryEntity)
	{
		if (withinEntities.Contains(entryEntity))
		{
			return;
		}

		SendEntity(entryEntity);
	}
}

void UBridgeComponent::OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 BodyIndex)
{
	withinEntities.Remove(Cast<AEntity>(OtherActor));
}

void UBridgeComponent::SendEntity(AEntity* entityToSend)
{
	if (otherBridgeComponent)
	{
		UBridgeComponent* bc = otherBridgeComponent->FindComponentByClass<UBridgeComponent>();
		bc->ReceiveEntity(entityToSend);
	}
}

void UBridgeComponent::ReceiveEntity(AEntity* entryEntity)
{
	// UEntityComponent* entityComponent = Cast<UEntityComponent>(GetOwner()->GetRootComponent());
	UE_LOG(LogTemp, Warning, TEXT("Receiving entity: %s"), *entryEntity->GetName());
	withinEntities.Add(entryEntity);
	entryEntity->OnMoveDirectionChange.Broadcast(GetHeadingVector(angleDirection));
	entryEntity->SetActorLocation(GetOwner()->GetActorLocation());
}