// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/MoveSpaceViewerComponent.h"

#include "Engine/World.h"
#include "GameFramework/Pawn.h"
#include "GameFramework/PlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Camera/CameraComponent.h"


#include "GameModes/LevelGameMode.h"

// Sets default values for this component's properties
UMoveSpaceViewerComponent::UMoveSpaceViewerComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UMoveSpaceViewerComponent::BeginPlay()
{
	Super::BeginPlay();

	if (Cast<APawn>(GetOwner()))
	{
		APlayerController* controller = GetWorld()->GetFirstPlayerController();
		controller->InputComponent->BindAxis("MoveRight", this, &UMoveSpaceViewerComponent::MoveRight);
		controller->InputComponent->BindAxis("MoveUp", this, &UMoveSpaceViewerComponent::MoveUp);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("SpaceViewerComponent must be attached to a Pawn class"))
	}
	
	gameMode = Cast<ALevelGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	camera = GetOwner()->FindComponentByClass<UCameraComponent>();
	if (!camera)
	{
		UE_LOG(LogTemp, Error, TEXT("Camera Component not found"));
		return;
	}

	// float height = camera->OrthoWidth / camera->AspectRatio;
	FRect worldLimits = gameMode->GetWorldLimits();

	float height = camera->OrthoWidth / camera->AspectRatio;
	float halfWidth = camera->OrthoWidth * 0.5f;
	float halfHeight = height * 0.5f;

	limits.x = worldLimits.x + (halfWidth);
	limits.y = worldLimits.y + (halfHeight);
	limits.w = worldLimits.w - (halfWidth * 2.0f);
	limits.h = worldLimits.h - (halfHeight * 2.0f);

	UE_LOG(LogTemp, Warning, TEXT("World Limits. %s"), *limits.ToString());
}


// Called every frame
void UMoveSpaceViewerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FVector velocity = FVector(moveX * rightSpeed, 0.0f, moveZ * upSpeed);
	FVector motion = velocity * DeltaTime;
	if (IsMovementAllowed(motion) || noBounds)
	{
		GetOwner()->AddActorWorldOffset(motion);
	}
}


bool UMoveSpaceViewerComponent::IsMovementAllowed(const FVector& motion)
{
	FVector newLocation = motion + GetOwner()->GetActorLocation();
	// return 	newLocation.X > limits.Key.X &&
	// 		newLocation.Z > limits.Key.Z &&
	// 		newLocation.X < limits.Value.X &&
	// 		newLocation.Z < limits.Value.Z;
	return limits.Contains(newLocation);
}


void UMoveSpaceViewerComponent::MoveRight(float value)
{
	moveX = value;
}

void UMoveSpaceViewerComponent::MoveUp(float value)
{
	moveZ = value;
}

void UMoveSpaceViewerComponent::BeginDestroy()
{
	Super::BeginDestroy();
	//  ???? I am not pretty sure whether what I did here is correct or not.
	// APlayerController* controller = GetWorld()->GetFirstPlayerController();
	// controller->InputComponent->AxisBindings.Remove(*moveRightBinding);
	// controller->InputComponent->AxisBindings.Remove(*moveUpBinding);
}