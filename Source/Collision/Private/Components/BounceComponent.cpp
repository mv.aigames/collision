// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/BounceComponent.h"

#include "Entity.h"
#include "Components/EntityComponent.h"

// Sets default values for this component's properties
UBounceComponent::UBounceComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UBounceComponent::BeginPlay()
{
	Super::BeginPlay();

	if (AEntity* entity = Cast<AEntity>(GetOwner()))
	{
		UE_LOG(LogTemp, Warning, TEXT("BounceComponent Interactive"));
		entity->OnCollision.AddDynamic(this, &UBounceComponent::Bounce);
	}
}


void UBounceComponent::Bounce(UPrimitiveComponent* HitComponent, AEntity* OtherActor, UPrimitiveComponent* OtherComponent, FVector impactVelocity, const FHitResult& Hit)
{
	// UE_LOG(LogTemp, Warning, TEXT("Bounce"));
	if (bouncedEntity == OtherActor)
	{
		return;
	}

	if (UEntityComponent* entityComponent = Cast<UEntityComponent>(GetOwner()->GetRootComponent()))
	{
		FVector velocity = impactVelocity;
		velocity.Y = 0.0f;
		FVector normal = Hit.Normal;
		UE_LOG(LogTemp, Warning, TEXT("Normal: %s"), *Hit.Normal.ToString());
		UE_LOG(LogTemp, Warning, TEXT("ImpactNormal: %s"), *Hit.ImpactNormal.ToString());
		UE_LOG(LogTemp, Warning, TEXT("Velocity: %s"), *velocity.ToString());
		normal.Y = 0.0f;
		// FVector direction = velocity.GetUnsafeNormal();
		FVector bounce = velocity - ((2.0f * FVector::DotProduct(velocity, normal.GetUnsafeNormal()) * normal.GetUnsafeNormal()));
		UE_LOG(LogTemp, Warning, TEXT("Bounce: %s"), *bounce.ToString());
		// // // Cast<AEntity>(GetOwner())->OnBounce.Broadcast(bounceDirection.GetUnsafeNormal() * velocity.Size());
		Cast<AEntity>(GetOwner())->OnBounce.Broadcast(bounce);
		// UE_LOG(LogTemp, Warning, TEXT("Bounce Ended"));
		// bouncedEntity = OtherActor;
	}
}