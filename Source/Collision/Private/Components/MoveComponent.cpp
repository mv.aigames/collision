// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/MoveComponent.h"

#include "Entity.h"

// Sets default values for this component's properties
UMoveComponent::UMoveComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
	// ...
}

void UMoveComponent::InitializeComponent()
{
	Super::InitializeComponent();

	if (AEntity* entity = Cast<AEntity>(GetOwner()))
	{
		entity->OnBounce.AddDynamic(this, &UMoveComponent::OnBounce);
		entity->OnPropulsion.AddDynamic(this, &UMoveComponent::OnPropulsion);
		entity->OnAddVelocity.AddDynamic(this, &UMoveComponent::OnAddVelocity);
		entity->OnMoveDirectionChange.AddDynamic(this, &UMoveComponent::OnDirectionChange);

	}
}

void UMoveComponent::OnDirectionChange(const FVector2D& newDirection)
{
	UE_LOG(LogTemp, Warning, TEXT("Direction changed"));
	velocity = FVector(newDirection.X, 0, newDirection.Y) * velocity.Size();
}

void UMoveComponent::OnAddVelocity(const FVector& deltaVelocity)
{
	velocity += deltaVelocity;
}


void UMoveComponent::OnBounce(const FVector& newVelocity)
{
	UE_LOG(LogTemp, Warning, TEXT("MoveComponent Bounce"));
	velocity = newVelocity;
}


void UMoveComponent::OnPropulsion(const FVector& newVelocity)
{
	UE_LOG(LogTemp, Warning, TEXT("UMoveComponent OnPropulsion"));
	velocity = newVelocity;
}


// Called when the game starts
void UMoveComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UMoveComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	AEntity* entity = Cast<AEntity>(GetOwner());
	if (shouldMove)
	{
		FHitResult hit;
		FVector motion = velocity * DeltaTime;
		entity->AddActorWorldOffset(motion, true, &hit);
		if (AEntity* otherEntity = Cast<AEntity>(hit.GetActor()))
		{
			// UE_LOG(LogTemp, Warning, TEXT("MoveComponent Velciity: %s"), *velocity.ToString());
			entity->OnCollision.Broadcast(Cast<UPrimitiveComponent>(entity->GetRootComponent()), otherEntity, Cast<UPrimitiveComponent>(otherEntity->GetRootComponent()), velocity, hit);
		}
	}
}

