// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/GravityComponent.h"

#include "CollisionChannelManagment.h"

#include "Collision/Public/Entity.h"
#include "CollisionChannelManagment.h"
#include "Components/EntityComponent.h"


UGravityComponent::UGravityComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
    SetSimulatePhysics(false);
    SetNotifyRigidBodyCollision(false);
    SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
    SetCollisionResponseToChannel(CollisionChannelManagment::GetPlayerSelectionChannel(), ECollisionResponse::ECR_Ignore);
}


void UGravityComponent::BeginPlay()
{
    Super::BeginPlay();

    SetComponentTickEnabled(false);

    OnComponentBeginOverlap.AddDynamic(this, &UGravityComponent::OnComponentOverlapBegin);
    OnComponentEndOverlap.AddDynamic(this, &UGravityComponent::ComponentOberlapEnd);

}

void UGravityComponent::TickComponent(float DeltaTime, ELevelTick TickMode, FActorComponentTickFunction* ActorComponentTickFunction)
{
    Super::TickComponent(DeltaTime, TickMode, ActorComponentTickFunction);

    for (int32 index = 0; index < entitiesToAbsorb.Num(); index++)
    {
        AEntity* entity = entitiesToAbsorb[index];
        FVector distance = GetOwner()->GetActorLocation() - entity->GetActorLocation();
        float influence = 1.0f - (distance.Size() / SphereRadius);
        FVector deltaVelocity = distance.GetUnsafeNormal() * interpolate3(0.0f, attractionForce, influence) * DeltaTime;
        deltaVelocity.Y = 0.0f;
        entity->OnAddVelocity.Broadcast(deltaVelocity);
    }
}

void UGravityComponent::OnCollided(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit)
{

}

void UGravityComponent::OnComponentOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult)
{
    UE_LOG(LogTemp, Warning, TEXT("Entity Entered"));
    SetComponentTickEnabled(true);
    entitiesToAbsorb.Add(Cast<AEntity>(OtherActor));

}

void UGravityComponent::ComponentOberlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex)
{
    UE_LOG(LogTemp, Warning, TEXT("Entity Exited"));
    entitiesToAbsorb.Remove(Cast<AEntity>(OtherActor));
    if (entitiesToAbsorb.Num() == 0)
    {
        SetComponentTickEnabled(false);
    }
}

template<typename T>
T UGravityComponent::interpolate3(T begin, T end, float t)
{
    float value = begin + ((end - begin) * t * t);
    return value;
}