// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/DirectionComponent.h"


#include "Entity.h"

// Sets default values for this component's properties
UDirectionComponent::UDirectionComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
	bWantsInitializeComponent = true;
}


void UDirectionComponent::InitializeComponent()
{
	Super::InitializeComponent();

	if (GetNumChildrenComponents() == 0 || GetNumChildrenComponents() > 1)
	{
		UE_LOG(LogTemp, Error, TEXT("There must be one child"));
		DestroyComponent();
		return;
	}

	if (AEntity* entity = Cast<AEntity>(GetOwner()))
	{
		entity->OnDirectionChange.AddDynamic(this, &UDirectionComponent::OnDirectionChange);
		staticMeshComponent = Cast<UStaticMeshComponent>(GetChildComponent(0));
	}
}

// Called when the game starts
void UDirectionComponent::BeginPlay()
{
	Super::BeginPlay();
	
	// staticMeshComponent->SetVisibility(false);
}


void UDirectionComponent::OnDirectionChange(const FVector2D& newDirection)
{
	// UE_LOG(LogTemp, Warning, TEXT("Direction Changes"));
	direction = newDirection;
	if (direction == FVector2D::ZeroVector)
	{
		staticMeshComponent->SetVisibility(false);
	}
	else
	{
		staticMeshComponent->SetVisibility(true);
		FVector position = FVector(direction.X, 0, direction.Y) * offset;
		staticMeshComponent->SetRelativeLocation(position);
		float angle = FMath::Atan2(newDirection.X, newDirection.Y) - FMath::Atan2(FVector2D::UnitVector.Y, 0);
		if (angle < 0)
		{
			angle += 2.0f * PI;
		}
		staticMeshComponent->SetRelativeRotation(FRotator(FMath::RadiansToDegrees(-angle), 0, 0));
	}
}

