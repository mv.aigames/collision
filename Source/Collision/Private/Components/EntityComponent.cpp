// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/EntityComponent.h"

#include "Entity.h"

UEntityComponent::UEntityComponent()
{
    PrimaryComponentTick.bCanEverTick = true;
    bWantsInitializeComponent = true;
    SetSimulatePhysics(true);
    SetEnableGravity(false);
    SetNotifyRigidBodyCollision(true);
}

void UEntityComponent::InitializeComponent()
{
    Super::InitializeComponent();
    AEntity* entity = Cast<AEntity>(GetOwner());
    UE_LOG(LogTemp, Warning, TEXT("InitializeComponent"));
    if (entity)
    {
        UE_LOG(LogTemp, Warning, TEXT("Adddynamic"));
        entity->OnPropulsion.AddDynamic(this, &UEntityComponent::Propulse);
        entity->OnBounce.AddDynamic(this, &UEntityComponent::Bounce);

    }

    BodyInstance.SetInstanceSimulatePhysics(true);
    

}


// void UEntityComponent::UpdateBodySetup()
// {

// }

// FPrimitiveSceneProxy*  UEntityComponent::CreateSceneProxy()
// {
//     return nullptr;
// }

// FBoxSphereBounds UEntityComponent::CalcBounds(const FTransform& LocalToWorld) const
// {
//     return FBoxSphereBounds();
// }

void UEntityComponent::TickComponent(float DeltaTime, ELevelTick TickMode, FActorComponentTickFunction* ActorComponentTickFunction)
{
    Super::TickComponent(DeltaTime, TickMode, ActorComponentTickFunction);


    // UE_LOG(LogTemp, Warning, TEXT("BdyInstance.IsDynamic: %s"), BodyInstance.IsDynamic() ? *FString("true"): *FString("false"));
}

void UEntityComponent::BeginPlay()
{
    Super::BeginPlay();
}

void UEntityComponent::Propulse(const FVector& propulsion)
{
    velocity = propulsion;
    
}

void UEntityComponent::Bounce(const FVector& newVelocity)
{
    velocity = newVelocity;
}