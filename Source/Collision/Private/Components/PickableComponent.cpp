// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PickableComponent.h"

#include "Components/ShapeComponent.h"

#include "CollisionChannelManagment.h"
#include "Entity.h"

// Sets default values for this component's properties
UPickableComponent::UPickableComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
	bWantsInitializeComponent = true;
}


void UPickableComponent::InitializeComponent()
{
	Super::InitializeComponent();

	if (GetNumChildrenComponents() != 0)
	{
		Cast<UPrimitiveComponent>(GetChildComponent(0))->OnComponentBeginOverlap.AddDynamic(this, &UPickableComponent::OnOverlap);
	}
}

// Called when the game starts
void UPickableComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
// void UPickableComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
// {
// 	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

// 	// ...
// }

void UPickableComponent::OnOverlap(UPrimitiveComponent* OverlappedCOmponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (AEntity* entity = Cast<AEntity>(OtherActor))
	{
		AEntity* ownerEntity = Cast<AEntity>(GetOwner());
		entity->OnPick.Broadcast(ownerEntity);
		if (pickedVFX)
		{
			AActor* vfx = GetWorld()->SpawnActor<AActor>(pickedVFX, GetOwner()->GetActorLocation(), FRotator());
		}
		ownerEntity->Destroy();
	}
}