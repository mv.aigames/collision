// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PlayerInputComponent.h"

#include "CollisionChannelManagment.h"
#include "Controllers/SpaceController.h"
#include "Components/InteractiveComponent.h"
#include "Entity.h"

#include "Engine/World.h"

// Sets default values for this component's properties
UPlayerInputComponent::UPlayerInputComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPlayerInputComponent::BeginPlay()
{
	Super::BeginPlay();
	controller = GetWorld()->GetFirstPlayerController();
	if (controller)
	{
		controller->InputComponent->BindAction("Selection", EInputEvent::IE_Pressed, this, &UPlayerInputComponent::Selection);
		controller->InputComponent->BindAction("Selection", EInputEvent::IE_Released, this, &UPlayerInputComponent::Release);
	}

}


// Called every frame
void UPlayerInputComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (interactiveComponent)
	{
		// UE_LOG(LogTemp, Warning, TEXT("PLayerInput Tick"));
		Cast<AEntity>(interactiveComponent->GetOwner())->OnDirectionChange.Broadcast(GetInputDirection());
	}
}


void UPlayerInputComponent::Selection()
{
	UE_LOG(LogTemp, Warning, TEXT("Selection"));
	FVector2D mousePosition;
	controller->GetMousePosition(mousePosition.X, mousePosition.Y);
	FCollisionQueryParams params;
	FHitResult hit;
	controller->GetHitResultAtScreenPosition(mousePosition, CollisionChannelManagment::GetPlayerSelectionChannel(), false, hit);

	if (hit.GetActor())
	{
		FVector worldPosition;
		FVector worldDirection;
		controller->DeprojectScreenPositionToWorld(mousePosition.X, mousePosition.Y, worldPosition, worldDirection);
		initialPosition = FVector2D(worldPosition.X, worldPosition.Z);
		interactiveComponent = Cast<UInteractiveComponent>(hit.GetComponent());
		SetComponentTickEnabled(true);
	}
}


void UPlayerInputComponent::Release()
{
	UE_LOG(LogTemp, Warning, TEXT("Release"));
	if (interactiveComponent)
	{
		UE_LOG(LogTemp, Warning, TEXT("Release Add Propulsion"));
		FVector2D fPosition;
		FVector worldPosition;
		FVector worldDirection;
		controller->GetMousePosition(fPosition.X, fPosition.Y);
		controller->DeprojectScreenPositionToWorld(fPosition.X, fPosition.Y, worldPosition, worldDirection);
		FVector2D diff = initialPosition - FVector2D(worldPosition.X, worldPosition.Z);
		FVector fPropulsion(diff.X, 0.0f, diff.Y);
		interactiveComponent->AddPropulsion(fPropulsion);
		SetComponentTickEnabled(false);
		Cast<AEntity>(interactiveComponent->GetOwner())->OnDirectionChange.Broadcast(FVector2D::ZeroVector);
	}
	interactiveComponent = nullptr;

}

FVector2D UPlayerInputComponent::GetInputDirection()
{
	if (!interactiveComponent)
	{
		return FVector2D::ZeroVector;
	}

	FVector2D mousePosition;
	controller->GetMousePosition(mousePosition.X, mousePosition.Y);
	FVector worldPosition;
	FVector worldDirection;
	controller->DeprojectScreenPositionToWorld(mousePosition.X, mousePosition.Y, worldPosition, worldDirection);
	
	FVector diff = (interactiveComponent->GetComponentLocation() - worldPosition);

	FVector2D direction(diff.X, diff.Z);

	return direction.GetSafeNormal();
}