// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PlanetOrbitComponent.h"

// Sets default values for this component's properties
UPlanetOrbitComponent::UPlanetOrbitComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UPlanetOrbitComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}

void UPlanetOrbitComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	FVector location = GetChildComponent(0)->GetRelativeLocation();
	float distance = FVector::Distance(location, FVector::ZeroVector);
	AddLocalRotation(FRotator(0.0f, (distance / orbitSpeed) * DeltaTime, 0.0f));
}

