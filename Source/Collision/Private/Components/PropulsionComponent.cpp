// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/PropulsionComponent.h"
#include "Entity.h"
#include "Components/EntityComponent.h"

// Sets default values for this component's properties
UPropulsionComponent::UPropulsionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	
}


// Called when the game starts
void UPropulsionComponent::BeginPlay()
{
	Super::BeginPlay();
	UE_LOG(LogTemp, Warning, TEXT("OnPropulsion"));

	// if (AEntity* entity = (AEntity*)GetOwner())
	if (AEntity* entity = Cast<AEntity>(GetOwner()))
	{
		// entity->OnBounce.AddDynamic(this, &UPropulsionComponent::Bounce);
		// entity->OnAddVelocity.AddDynamic(this, &UPropulsionComponent::AddVelocity);
		// entity->OnPropulsion.Broadcast(propulsionForce * propulsionDirection);
		UE_LOG(LogTemp, Warning, TEXT("Added OnPropulsion"));
		entity->OnAddPropulsion.AddDynamic(this, &UPropulsionComponent::AddPropulsion);
	}
	
}


// Called every frame
void UPropulsionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	// UE_LOG(LogTemp, Warning, TEXT("Tick Component"));
	// ...
	// Cast<AEntity>(GetOwner())->OnPropulsion.Broadcast(propulsionForce * propulsionDirection * DeltaTime);
	// UEntityComponent* EntityComponent = Cast<UEntityComponent>(GetOwner()->GetRootComponent());
	// // UE_LOG(LogTemp, Warning, TEXT("Velocity: %s"), *(propulsionForce * propulsionDirection).ToString());
	// // EntityComponent->SetPhysicsLinearVelocity(propulsionForce * propulsionDirection);
	// EntityComponent->BodyInstance.SetLinearVelocity(propulsionForce * propulsionDirection, false);
	
}

void UPropulsionComponent::Bounce(const FVector& newVelocity)
{
	// UE_LOG(LogTemp, Warning, TEXT("In Bounce"));
	propulsionForce = newVelocity.Size();
	propulsionDirection = newVelocity.GetUnsafeNormal();
}

void UPropulsionComponent::AddPropulsion(const FVector& addedPropulsion)
{
	// UE_LOG(LogTemp, Warning, TEXT("Add Velocity"));
	Cast<AEntity>(GetOwner())->OnPropulsion.Broadcast(addedPropulsion * propulsionFactor);
}