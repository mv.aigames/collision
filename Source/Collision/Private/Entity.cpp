// Fill out your copyright notice in the Description page of Project Settings.


#include "Entity.h"

#include "Collision/Public/Components/EntityComponent.h"

// Sets default values
AEntity::AEntity()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	entityComponent = CreateDefaultSubobject<UEntityComponent>(TEXT("Interactive Component"));
	RootComponent = entityComponent;
}

// Called when the game starts or when spawned
void AEntity::BeginPlay()
{
	Super::BeginPlay();
	
}


void AEntity::Tick(float DeltaTime) 
{
	Super::Tick(DeltaTime);
}
