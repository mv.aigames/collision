// Fill out your copyright notice in the Description page of Project Settings.


#include "Utils/Rect.h"

FRect::FRect()
{
}

FRect::FRect(float p_x, float p_y, float p_w, float p_h): x{p_x}, y{p_y}, w{p_w}, h{p_h}
{

}

bool FRect::Contains(const FVector& point)
{
    return  point.X > x &&
            point.X < x + w &&
            point.Z > y &&
            point.Z < y + h;
}

FString FRect::ToString()
{
    return FString::Printf(TEXT("X=%3.3f Y=%3.3f W=%3.3f H=%3.3f"), x, y, w, h);
}