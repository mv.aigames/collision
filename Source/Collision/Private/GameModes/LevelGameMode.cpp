// Fill out your copyright notice in the Description page of Project Settings.


#include "GameModes/LevelGameMode.h"

#include "Entity.h"
#include "Components/GoalComponent.h"

#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"




void ALevelGameMode::BeginPlay()
{
    Super::BeginPlay();

    TArray<AActor*> entities;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEntity::StaticClass(), entities);

    UE_LOG(LogTemp, Warning, TEXT("Entities: %d"), entities.Num());
    for (int32 index = 0; index < entities.Num(); index++)
    {
        if (AEntity* entity = Cast<AEntity>(entities[index]))
        {
            entity->OnGoal.AddDynamic(this, &ALevelGameMode::GoalReached);
        }
    }

    OnDeadZone.AddDynamic(this, &ALevelGameMode::DeadZoneReached);

    limits = GetLimits();

    UE_LOG(LogTemp, Warning, TEXT("Limits. %s"), *limits.ToString());
}


void ALevelGameMode::LevelCompleted()
{
    PostLevelCompleted();
}

void ALevelGameMode::GoalReached()
{
    LevelCompleted();
}

void ALevelGameMode::DeadZoneReached()
{
    PostDeadZoneReached();
}

FRect ALevelGameMode::GetLimits()
{
    float minX = 0.0f;
    float minZ = 0.0f;
    float maxX = 0.0f;
    float maxZ = 0.0f;

    TArray<AActor*> entities;
    UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEntity::StaticClass(), entities);

    for (int32 index = 0; index < entities.Num(); index++)
    {
        FVector location = entities[index]->GetActorLocation();
        
        minX = location.X < minX ? location.X : minX;
        minZ = location.Z < minZ ? location.Z : minZ;
        maxX = location.X > maxX ? location.X : maxX;
        maxZ = location.Z > maxZ ? location.Z : maxZ;
    }

    // return FIntRect(FVector(minX, 0.0f, minZ), FVector(maxX, 0.0f, maxZ));
    return FRect(minX, minZ, FMath::Abs(maxX - minX), FMath::Abs(maxZ - minZ));
}

FRect ALevelGameMode::GetWorldLimits()
{
    return limits;
}

bool ALevelGameMode::IsInsideArea(const FVector& point)
{
    // return  point.X > limits.Key.X &&
    //         point.X < limits.Value.X &&
    //         point.Z < limits.Key.Z &&
    //         point.Z > limits.Value.Z;
    return true;
}