// Copyright Epic Games, Inc. All Rights Reserved.


#include "CollisionGameModeBase.h"

TArray<FSpaceSectionData> ACollisionGameModeBase::GetSpaceSections()
{
    if (spaceSectionData)
    {
        TArray<FSpaceSectionData> sections;
        TArray<FSpaceSectionData*> outRows;
        FString context;
        spaceSectionData->GetAllRows(*context, outRows);
        TArray<FName> names = spaceSectionData->GetRowNames();
        UE_LOG(LogTemp, Warning, TEXT("row size: %d"), outRows.Num())
        for (int32 index = 0; index < names.Num(); index++)
        {
            
        }
    }
    return TArray<FSpaceSectionData>();
}

TArray<FLevelData> ACollisionGameModeBase::GetLevelData(FSpaceSectionData& data)
{
    return TArray<FLevelData>();
}