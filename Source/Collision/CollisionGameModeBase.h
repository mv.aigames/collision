// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Engine/DataTable.h"
#include "CollisionGameModeBase.generated.h"

USTRUCT(BlueprintType)
struct FSpaceSectionData: public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Space")
	int32 id;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Space")
	float distance;
};

USTRUCT(BlueprintType)
struct FLevelData
{
	GENERATED_BODY()

	UPROPERTY()
	int32 id;
};

/**
 * 
 */
UCLASS()
class COLLISION_API ACollisionGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	private:
		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Data", meta = (AllowPrivateAccess = "true"))
		UDataTable* spaceSectionData;

	public:
		UFUNCTION(BlueprintCallable)
		TArray<FSpaceSectionData> GetSpaceSections();
		UFUNCTION(BlueprintCallable)
		TArray<FLevelData> GetLevelData(FSpaceSectionData& data);
};
