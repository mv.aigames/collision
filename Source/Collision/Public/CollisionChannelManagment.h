// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class COLLISION_API CollisionChannelManagment
{
public:
	CollisionChannelManagment();
	~CollisionChannelManagment();

	static ECollisionChannel GetPlayerSelectionChannel() { return ECollisionChannel::ECC_GameTraceChannel1; }
};
