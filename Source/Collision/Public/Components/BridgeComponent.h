// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BridgeComponent.generated.h"

class AEntity;

UCLASS( ClassGroup=(Entity), meta=(BlueprintSpawnableComponent), Blueprintable, BlueprintType )
class COLLISION_API UBridgeComponent : public USceneComponent
{
	GENERATED_BODY()

	public:	
		// Sets default values for this component's properties
		UBridgeComponent();
		UFUNCTION()
		void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 BodyIndex, bool FromSweep, const FHitResult& Hit);
		UFUNCTION()
		void OnEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 BodyIndex);
		

		void SendEntity(AEntity* entity);
		void ReceiveEntity(AEntity* entity);

	protected:
		// Called when the game starts
		virtual void BeginPlay() override;
		virtual void OnRegister() override;

	public:	
		// Called every frame
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	private:
		UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category="Bridge", meta=(AllowPrivateAccess = "true"))
		AEntity* otherBridgeComponent;

		TArray<AEntity*> withinEntities;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Bridge", meta = (AllowPrivateAccess = "true"))
		float angleDirection = 0;

		AEntity* entity;

		FVector2D GetHeadingVector(float degrees)
		{
			float radians = FMath::DegreesToRadians(degrees);
			return FVector2D(FMath::Cos(radians), FMath::Sin(radians));
		}
		
};
