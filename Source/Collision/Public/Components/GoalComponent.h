// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GoalComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup=(Entity), BlueprintType, Blueprintable)
class COLLISION_API UGoalComponent : public USphereComponent
{
	GENERATED_BODY()
	
	protected:
		UGoalComponent();
		virtual void BeginPlay() override;

	public:
		UFUNCTION()
		void OverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);
};
