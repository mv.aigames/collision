// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "DirectionComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class COLLISION_API UDirectionComponent : public USceneComponent
{
	GENERATED_BODY()

	public:	
		UDirectionComponent();
		UFUNCTION()
		void OnDirectionChange(const FVector2D& newDirection);

	protected:
		virtual void InitializeComponent() override;
		virtual void BeginPlay() override;
	
	private:
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh", meta = (AllowPrivateAccess = "true"))
		UStaticMeshComponent* staticMeshComponent;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Direction", meta = (AllowPrivateAccess = "true"))
		FVector2D direction;

		UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Direction", meta = (AllowPrivateAccess = "true"))
		float offset = 100.0f;
};
