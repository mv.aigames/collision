// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperSpriteComponent.h"
#include "SpriteRotateComponent.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class COLLISION_API USpriteRotateComponent : public USceneComponent
{
	GENERATED_BODY()

	public:
		USpriteRotateComponent();
		virtual void TickComponent(float DeltaTime, ELevelTick TickMode, FActorComponentTickFunction* ActorComponentTickFunction) override;

	private:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Rotation", meta = (AllowPrivateAccess = "true"))
		float rotationSpeed = 0.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sprite", meta = (AllowPrivateAccess))
		UPaperSpriteComponent* spriteSubClass;
	
};
