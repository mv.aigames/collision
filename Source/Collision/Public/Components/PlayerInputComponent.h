// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PlayerInputComponent.generated.h"


class UInteractiveComponent;

UCLASS( ClassGroup=(SpaceViewer), meta=(BlueprintSpawnableComponent) )
class COLLISION_API UPlayerInputComponent : public UActorComponent
{
	GENERATED_BODY()

	public:	
		// Sets default values for this component's properties
		UPlayerInputComponent();

		UFUNCTION()
		void Selection();
		UFUNCTION()
		void Release();

	protected:
		// Called when the game starts
		virtual void BeginPlay() override;

	public:	
		// Called every frame
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	private:
		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		UInteractiveComponent* interactiveComponent = nullptr;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		FVector2D initialPosition;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		FVector2D finalPosition;

		APlayerController* controller;

		FVector2D GetInputDirection();
};
