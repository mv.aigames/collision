// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PickableComponent.generated.h"


class AEntity;
class UShapeComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class COLLISION_API UPickableComponent : public USceneComponent
{
	GENERATED_BODY()

	public:	
		// Sets default values for this component's properties
		UPickableComponent();
		virtual void InitializeComponent() override;
		// virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		UFUNCTION()
		void OnOverlap(UPrimitiveComponent* OverlappedCOmponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);


	protected:
		// Called when the game starts
		virtual void BeginPlay() override;

	private:
		UPROPERTY(EditDefaultsOnly, Category = "VFX", meta = (AllowPrivateAccess = "true"))
		TSubclassOf<AActor> pickedVFX;	
};
