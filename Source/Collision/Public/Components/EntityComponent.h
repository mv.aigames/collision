// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "EntityComponent.generated.h"

/**
 * 
 */
UCLASS()
class COLLISION_API UEntityComponent : public UStaticMeshComponent
{
	GENERATED_BODY()
	
	public:
		UEntityComponent();
		UFUNCTION()
		void Propulse(const FVector& propulsion);
		UFUNCTION()
		void Bounce(const FVector& newVelocity);
		virtual void InitializeComponent() override;
		virtual void TickComponent(float Deltatime, ELevelTick TickMode, FActorComponentTickFunction* ActorComponentTickFunction) override;

	protected:
		virtual void BeginPlay() override;
	
	private:
	FVector velocity;

};
