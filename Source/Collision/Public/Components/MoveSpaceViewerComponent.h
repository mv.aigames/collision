// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Utils/Rect.h"

#include "MoveSpaceViewerComponent.generated.h"


class ALevelGameMode;
class UCameraComponent;

/*
* It must be a child of a APawn.
*/
UCLASS( ClassGroup=(SpaceViewer), meta=(BlueprintSpawnableComponent) )
class COLLISION_API UMoveSpaceViewerComponent : public UActorComponent
{
	GENERATED_BODY()

	public:	
		// Sets default values for this component's properties
		UMoveSpaceViewerComponent();

		// UFUNCTION()
		// void Selection();

		UFUNCTION()
		void MoveRight(float value);

		UFUNCTION()
		void MoveUp(float value);

		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		virtual void BeginDestroy() override;

	protected:
		// Called when the game starts
		virtual void BeginPlay() override;
	

	private:

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Move", meta = (AllowPrivateAccess = "true"))
		float rightSpeed = 1000.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Move", meta = (AllowPrivateAccess = "true"))
		float upSpeed = 1000.0f;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Move", meta = (AllowPrivateAccess = "true"))
		float moveX = 0.0f;

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Move", meta = (AllowPrivateAccess = "true"))
		float moveZ = 0.0f;

		FInputAxisBinding moveRightBinding;
		FInputAxisBinding moveUpBinding;
		UCameraComponent* camera;

		ALevelGameMode* gameMode;
		bool IsMovementAllowed(const FVector& motion);

		FRect limits;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Move", meta = (AllowPrivateAccess = "true"))
		bool noBounds = true;
};
