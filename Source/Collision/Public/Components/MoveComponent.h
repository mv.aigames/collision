// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MoveComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class COLLISION_API UMoveComponent : public UActorComponent
{
	GENERATED_BODY()

	public:	
		// Sets default values for this component's properties
		UMoveComponent();
		virtual void InitializeComponent() override;
		UFUNCTION()
		void OnBounce(const FVector& newVelocity);

		UFUNCTION()
		void OnPropulsion(const FVector& newVelocity);

		UFUNCTION()
		void OnAddVelocity(const FVector& deltaVelocity);

		UFUNCTION()
		void OnDirectionChange(const FVector2D& newDirection);

	protected:
		// Called when the game starts
		virtual void BeginPlay() override;
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	private:

		UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement", meta = (AllowPrivateAccess = "true"))
		FVector velocity;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement", meta = (AllowPrivateAccess = "true"))
		bool shouldMove = true;
		
};
