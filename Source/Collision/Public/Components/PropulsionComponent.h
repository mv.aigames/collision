// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PropulsionComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable )
class COLLISION_API UPropulsionComponent : public UActorComponent
{
	GENERATED_BODY()

	private:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Propulsion", meta = (AllowPrivateAccess = "true"))
		float propulsionForce = 1000.0f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Propulsion", meta = (AllowPrivateAccess = "true"))
		float propulsionFactor = 3.0f;

	public:	
		// Sets default values for this component's properties
		UPropulsionComponent();
		// Called every frame
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
		UFUNCTION()
		void Bounce(const FVector& newVelocity);
		UFUNCTION()
		void AddPropulsion(const FVector& DeltaVelocity);
		void SetPropulsionFactor(float value) { propulsionFactor = value; }

	protected:
		// Called when the game starts
		virtual void BeginPlay() override;

	private:
		FVector propulsionDirection = FVector(-1, 0, 0);
};
