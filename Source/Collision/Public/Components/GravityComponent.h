// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "GravityComponent.generated.h"


class AEntity;
/**
 * 
 */
UCLASS(BLueprintable)
class COLLISION_API UGravityComponent : public USphereComponent
{
	GENERATED_BODY()

	private:
		UPROPERTY(EditAnywhere, BlueprintReadWrite, category = "Gravity", meta = (AllowPrivateAccess = "true"))
		float attractionForce = 100.0f;
	
	public:
		UGravityComponent();
		virtual void BeginPlay() override;
		virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ActorComponentTickFunction) override;
	
		UFUNCTION()
		void OnCollided(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);

		UFUNCTION()
		void OnComponentOverlapBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool FromSweep, const FHitResult& SweepResult);


		UFUNCTION()
		void ComponentOberlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex);

		// UFUNCTION()
		// void MarkToDestroy();
	
	private:
		TArray<AEntity*> entitiesToAbsorb;

		template<typename T>
		T interpolate3(T begin, T end, float value);
};
