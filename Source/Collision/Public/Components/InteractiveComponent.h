// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "InteractiveComponent.generated.h"


class AEntity;

/**
 * This class is the interface between the entity and the player. All the player actions will be passed through here
 */
UCLASS(ClassGroup=(Entity), meta=(BlueprintSpawnableComponent), Blueprintable)
class COLLISION_API UInteractiveComponent : public USphereComponent
{
	GENERATED_BODY()

	public:
		UInteractiveComponent();

		virtual void InitializeComponent() override;
		UFUNCTION()
		void OnCursorOverBegin(UPrimitiveComponent* primitiveComponent);

		UFUNCTION()
		void OnCursorOverEnd(UPrimitiveComponent* primitiveComponent);

		UFUNCTION()
		void OnClickPressed(UPrimitiveComponent* primitiveComponent, FKey ButtonPressed);

		UFUNCTION()
		void OnClickReleased(UPrimitiveComponent* primitiveComponent, FKey ButtonReleased);

		void AddPropulsion(const FVector& addedPropulsion);
	
	private:
		AEntity* entity;
};
