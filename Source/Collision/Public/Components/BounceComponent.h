// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "BounceComponent.generated.h"


UCLASS( ClassGroup=(Entity), meta=(BlueprintSpawnableComponent), Blueprintable )
class COLLISION_API UBounceComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UBounceComponent();

	UFUNCTION()
	void Bounce(UPrimitiveComponent* HitComponent, AEntity* OtherActor, UPrimitiveComponent* OtherComponent, FVector velocity, const FHitResult& Hit);
	AActor* bouncedEntity = nullptr;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

		
};
