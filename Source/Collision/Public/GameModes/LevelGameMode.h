// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Collision/CollisionGameModeBase.h"
#include "Utils/Rect.h"
#include "LevelGameMode.generated.h"




DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDeadZoneSignature);
/**
 * 
 */
UCLASS()
class COLLISION_API ALevelGameMode : public ACollisionGameModeBase
{
	GENERATED_BODY()
	


	public:
		UFUNCTION(BlueprintCallable)
		void LevelCompleted();
		
		UFUNCTION(BlueprintImplementableEvent)
		void PostLevelCompleted();

		FDeadZoneSignature OnDeadZone;
	
		UFUNCTION(BlueprintImplementableEvent)
		void PostDeadZoneReached();

		bool IsInsideArea(const FVector& point);

		
		FRect GetWorldLimits();


	protected:
		virtual void BeginPlay() override;
	
	
	private:

		UFUNCTION()
		void GoalReached();

		UFUNCTION()
		void DeadZoneReached();

		FRect GetLimits();

		FRect limits;
};
