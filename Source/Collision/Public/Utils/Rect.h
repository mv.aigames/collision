// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"



/**
 * 
 */
struct FRect
{

	float x;
	float y;
	float w;
	float h;
	FRect();
	FRect(const FRect&) = default;
	FRect(float p_x, float p_y, float p_w, float p_h);

	FRect& operator=(const FRect&) = default;
	FRect& operator=(FRect&&) = default;

	bool Contains(const FVector& point);
	FString ToString();

};
