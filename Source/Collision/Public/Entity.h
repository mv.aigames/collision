// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/CapsuleComponent.h"
#include "Entity.generated.h"

// DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FCollisionSignature, AEntity*, otherEntity, FVector, Normal);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPropulsionSignature, const FVector&, propulsion);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAddPropulsionSignature, const FVector&, addedPropulsion);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBounceSignature, const FVector&, newVelocity);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAddVelocitySignature, const FVector&, deltaVelocity);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FiveParams(FCollisionSignature, UPrimitiveComponent*, collisionComponent, AEntity*, otherEntity, UPrimitiveComponent*, otherComponent, FVector, velocity, const FHitResult&, Hit);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPickSignature, AEntity*, entity);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FGoalSignature);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDirectionChangeSignature, const FVector2D&, newDirection);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FMoveDirectionChangeSignature, const FVector2D&, newDirection);



class UEntityComponent;

UCLASS(BlueprintType, Blueprintable)
class COLLISION_API AEntity : public AActor
{
	GENERATED_BODY()
	private:
		
		UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "RootComponent", meta = (AllowPrivateAccess = "true"))
		UEntityComponent* entityComponent;
		
	public:	
		// Sets default values for this actor's properties
		AEntity();

		// FCollisionSignature OnCollided;
		FPropulsionSignature OnPropulsion;
		FAddPropulsionSignature OnAddPropulsion;
		FBounceSignature OnBounce;
		FAddVelocitySignature OnAddVelocity;
		FCollisionSignature OnCollision;

		UPROPERTY(BlueprintAssignable)
		FPickSignature OnPick;

		UPROPERTY(BlueprintAssignable, meta = (DisplayName = "Goal Event"))
		FGoalSignature OnGoal;

		FDirectionChangeSignature OnDirectionChange;

		FMoveDirectionChangeSignature OnMoveDirectionChange;

	protected:
		// Called when the game starts or when spawned
		virtual void BeginPlay() override;
		virtual void Tick(float DeltaTime) override;

};
